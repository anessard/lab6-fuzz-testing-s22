import { calculateBonuses } from './bonus-system.js';



describe('Bonus system', () => {
    const testAmmount = 1000;  

    it('bonus is calculated correctly', () => {
      expect(calculateBonuses('Standard', 1)).toEqual(0.05 * 1);
      expect(calculateBonuses('Premium', 1)).toEqual(0.1* 1);
      expect(calculateBonuses('Diamond', 1)).toEqual(0.2 * 1);

      expect(calculateBonuses('Standard', 10000)).toEqual(0.05 * 1.5);
      expect(calculateBonuses('Premium', 10000)).toEqual(0.1 * 1.5);
      expect(calculateBonuses('Diamond', 10000)).toEqual(0.2 * 1.5);

      expect(calculateBonuses('Standard', 50000)).toEqual(0.05 * 2);
      expect(calculateBonuses('Premium',  50000)).toEqual(0.1 * 2);
      expect(calculateBonuses('Diamond',  50000)).toEqual(0.2 * 2);

      expect(calculateBonuses('Standard', 100000)).toEqual(0.05 * 2.5);
      expect(calculateBonuses('Premium',  100000)).toEqual(0.1 * 2.5);
      expect(calculateBonuses('Diamond',  100000)).toEqual(0.2 * 2.5);


    })
    
    it('correctly handles invalid names', () => {
      expect(calculateBonuses('Invalid', testAmmount)).toBe(0.0);
    });
  });