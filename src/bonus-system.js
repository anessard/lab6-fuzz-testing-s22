const bonus_limit = 20000;

//This is a function to calculate amount of bonuses, depending on the amount
//of money that are being sent to account and programm of the user
export const calculateBonuses = (program, amount) => {
    const multiplier = program === "Standard" ? 0.05 :
                            program === "Premium" ? 0.1 :
                                program === "Diamond" ? 0.2 : 0;
    const bonus = amount < 10000 ? 1 :
                    amount < 50000 ? 1.5 :
                        amount < 100000 ? 2 : 2.5;

    return (bonus * multiplier);
};

