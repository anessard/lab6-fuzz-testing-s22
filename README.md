# Lab6 -- Fuzz testing (and a bit of mutation testing)


## Introduction

Hey there, today you won't need to develop CI(finally you can relax a bit). We are going to walk through a fuzz and mutation testing. Both those testing approaches are not that much popular comparing to the one's we've reviewed previously, but in the same time they are really useful. Don't forget to ask yourself each time before you deciding to test something, do you really need to use this testing method, and if answer is yes, answer how much resourses are you ready to use for testing. ***Let's roll!***

## Fuzz testing

You should already know what is the fuzz testing, because you had your lectures, but fuzz testing is basically generating different anomaly data samples and checking if your application responds to the anomalies as it should.


## Mutation testing

Mutation testing is a very good tool to check your own tests. It is changing different conditions/cycles and checking if some of the tests are failing because of it. If some tests are breaking, then nice job, you've developped a good testing suite.


## Lab

Finally, we may forget our old Java project, let's use something mainstream now, what about TS?
1. Create your fork of the `
Lab6 - Fuzz testing
` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/lab6-fuzz-testing-s22)
2. Now, let's run mutation testing to check if testing that we already have is enough, you can install lib with this command:
```sh
npm install -g stryker-cli
```
Then in directory of the project:
```sh
stryker init
```
Now let's install and configure Stryker, based on our project.
To run stryker you should type `stryker run`, it will show you something like this:

File             | % score | # killed | # timeout | # survived | # no cov | # error |
-----------------|---------|----------|-----------|------------|----------|---------|
All files        |   29.46 |       33 |         0 |         79 |        0 |       0 |
 bonus-system.js |    0.00 |        0 |         0 |         26 |        0 |       0 |
 calculator.js   |   73.33 |       33 |         0 |         12 |        0 |       0 |
 commutator.js   |    0.00 |        0 |         0 |         41 |        0 |       0 |

As you can see 73% of mutants were killed, we can look on the detailed report on bugs in the logs.
3. Fuzz testing, here we go, let's test our API from the previous lab, using fuzz testing plugin built into quite popular load testing tool - artillery, more info [here](https://github.com/artilleryio/artillery-plugin-fuzzer). We should install artillery and plugin-fuzzer first with this commands:
```sh
sudo npm install -g artillery@1.7.8
npm install -g artillery-plugin-fuzzer
```
Now, let's write our config file to run fuzz testing:
```yaml
config:
  target: "https://script.google.com/macros/s/{your_key}"
  phases:
    - duration: 10
      arrivalCount: 100
  plugins:
    fuzzer: {}
scenarios:
  - name: "Fuzz some stuff"
    flow:
      - get:
          url: "/exec?service=calculatePrice&email=golden&type={{naughtyString}}&plan=minute&distance=100&planned_distance=100&time=110&planned_time=100&inno_discount=yes"
      - log: "***** naughtyString = {{ naughtyString }}"
```
This way we are fuzz testing just 1 parameter(type). Let's tun out tests with this command:
```sh
artillery run example.yaml
```
To simplify your testing and to understand exactly what requests are breaking, you should do like this, create `control.js` file in the same directory with `example.yaml` with this script:
```js
module.exports = {
    logStatus: logStatus
}


function logStatus(requestParams, response, context, ee, next) {
    console.log(response.statusCode);
    return next(); // MUST be called for the scenario to continue
}
```
Then, edit your yaml like this, so it will use your script to display response statusCodes.
```yaml
config:
  target: "https://script.google.com/macros/s/AKfycbwI_VT8ONTc6mL7CYzILU8n7BxF1pa0BD9L-X5ta6hMBoscDfw0jvpD8g"
  phases:
    - duration: 10
      arrivalCount: 100
  plugins:
    fuzzer: {}
  processor: "./control.js"
scenarios:
  - name: "Fuzz some stuff"
    flow:
      - get:
          url: "/exec?service=calculatePrice&email=golden&type=budget&plan=minute&distance={{naughtyString}}&planned_distance=100&time=110&planned_time=100&inno_discount=yes"
          afterResponse: "logStatus"
      - log: "***** naughtyString = {{ naughtyString }}"
```
And finally, once again run:
```sh
artillery run example.yaml
```

## Homework

As a homework you will need to develop tests for function `calculateBonuses`(in `src/bonus-system.js`), then you should be sure that tests are killing 100% of the mutants. After it, you will need to create `.gitlab-ci.yml` and add your tests there.Plus you should attach screenshot of `stryker run` execution.
**Lab is counted as done, if pipelines are passing. and tests are developped**


Report


![](/lab6-fuzz-testing-s22/report.jpg)